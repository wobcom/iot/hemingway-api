# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: locator/v1/locator.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='locator/v1/locator.proto',
  package='locator.v1',
  syntax='proto3',
  serialized_options=b'Z;gitlab.com/wobcom/iot/hemingway-api/go/locator/v1;locatorv1',
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x18locator/v1/locator.proto\x12\nlocator.v1\"\x8f\x01\n\x07\x41\x64\x64ress\x12!\n\x0c\x61\x64\x64ress_line\x18\x01 \x01(\tR\x0b\x61\x64\x64ressLine\x12,\n\x12\x61\x64\x64ress_line_extra\x18\x02 \x03(\tR\x10\x61\x64\x64ressLineExtra\x12\x1f\n\x0bpostal_code\x18\x03 \x01(\tR\npostalCode\x12\x12\n\x04\x63ity\x18\x04 \x01(\tR\x04\x63ity\"G\n\x0b\x43oordinates\x12\x1a\n\x08latitude\x18\x01 \x01(\x02R\x08latitude\x12\x1c\n\tlongitude\x18\x02 \x01(\x02R\tlongitude\"\'\n\nGetRequest\x12\x19\n\x08meter_id\x18\x01 \x01(\tR\x07meterId\"w\n\x0bGetResponse\x12-\n\x07\x61\x64\x64ress\x18\x01 \x01(\x0b\x32\x13.locator.v1.AddressR\x07\x61\x64\x64ress\x12\x39\n\x0b\x63oordinates\x18\x02 \x01(\x0b\x32\x17.locator.v1.CoordinatesR\x0b\x63oordinates2H\n\x0eLocatorService\x12\x36\n\x03Get\x12\x16.locator.v1.GetRequest\x1a\x17.locator.v1.GetResponseB=Z;gitlab.com/wobcom/iot/hemingway-api/go/locator/v1;locatorv1b\x06proto3'
)




_ADDRESS = _descriptor.Descriptor(
  name='Address',
  full_name='locator.v1.Address',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='address_line', full_name='locator.v1.Address.address_line', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='addressLine', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='address_line_extra', full_name='locator.v1.Address.address_line_extra', index=1,
      number=2, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='addressLineExtra', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='postal_code', full_name='locator.v1.Address.postal_code', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='postalCode', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='city', full_name='locator.v1.Address.city', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='city', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=41,
  serialized_end=184,
)


_COORDINATES = _descriptor.Descriptor(
  name='Coordinates',
  full_name='locator.v1.Coordinates',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='latitude', full_name='locator.v1.Coordinates.latitude', index=0,
      number=1, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='latitude', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='longitude', full_name='locator.v1.Coordinates.longitude', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='longitude', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=186,
  serialized_end=257,
)


_GETREQUEST = _descriptor.Descriptor(
  name='GetRequest',
  full_name='locator.v1.GetRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='meter_id', full_name='locator.v1.GetRequest.meter_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='meterId', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=259,
  serialized_end=298,
)


_GETRESPONSE = _descriptor.Descriptor(
  name='GetResponse',
  full_name='locator.v1.GetResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='address', full_name='locator.v1.GetResponse.address', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='address', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='coordinates', full_name='locator.v1.GetResponse.coordinates', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, json_name='coordinates', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=300,
  serialized_end=419,
)

_GETRESPONSE.fields_by_name['address'].message_type = _ADDRESS
_GETRESPONSE.fields_by_name['coordinates'].message_type = _COORDINATES
DESCRIPTOR.message_types_by_name['Address'] = _ADDRESS
DESCRIPTOR.message_types_by_name['Coordinates'] = _COORDINATES
DESCRIPTOR.message_types_by_name['GetRequest'] = _GETREQUEST
DESCRIPTOR.message_types_by_name['GetResponse'] = _GETRESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Address = _reflection.GeneratedProtocolMessageType('Address', (_message.Message,), {
  'DESCRIPTOR' : _ADDRESS,
  '__module__' : 'locator.v1.locator_pb2'
  # @@protoc_insertion_point(class_scope:locator.v1.Address)
  })
_sym_db.RegisterMessage(Address)

Coordinates = _reflection.GeneratedProtocolMessageType('Coordinates', (_message.Message,), {
  'DESCRIPTOR' : _COORDINATES,
  '__module__' : 'locator.v1.locator_pb2'
  # @@protoc_insertion_point(class_scope:locator.v1.Coordinates)
  })
_sym_db.RegisterMessage(Coordinates)

GetRequest = _reflection.GeneratedProtocolMessageType('GetRequest', (_message.Message,), {
  'DESCRIPTOR' : _GETREQUEST,
  '__module__' : 'locator.v1.locator_pb2'
  # @@protoc_insertion_point(class_scope:locator.v1.GetRequest)
  })
_sym_db.RegisterMessage(GetRequest)

GetResponse = _reflection.GeneratedProtocolMessageType('GetResponse', (_message.Message,), {
  'DESCRIPTOR' : _GETRESPONSE,
  '__module__' : 'locator.v1.locator_pb2'
  # @@protoc_insertion_point(class_scope:locator.v1.GetResponse)
  })
_sym_db.RegisterMessage(GetResponse)


DESCRIPTOR._options = None

_LOCATORSERVICE = _descriptor.ServiceDescriptor(
  name='LocatorService',
  full_name='locator.v1.LocatorService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=421,
  serialized_end=493,
  methods=[
  _descriptor.MethodDescriptor(
    name='Get',
    full_name='locator.v1.LocatorService.Get',
    index=0,
    containing_service=None,
    input_type=_GETREQUEST,
    output_type=_GETRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_LOCATORSERVICE)

DESCRIPTOR.services_by_name['LocatorService'] = _LOCATORSERVICE

# @@protoc_insertion_point(module_scope)
