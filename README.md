<div align="center">

# hemingway-api

The public API of the <a href="https://wobcom.de"><img src="./wobcom-badge.png"></a> SmartMetering Integration

<img src="./README.gif">

</div>

## python

[grpclib](https://grpclib.readthedocs.io/en/latest/) is used here.
