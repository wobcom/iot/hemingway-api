module gitlab.com/wobcom/iot/hemingway-api/go

go 1.18

require (
	github.com/bufbuild/connect-go v0.4.0
	github.com/google/go-cmp v0.5.8
	github.com/matryer/is v1.4.0
	google.golang.org/protobuf v1.28.1
)
