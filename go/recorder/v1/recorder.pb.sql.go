package recorderv1

import (
	"database/sql/driver"
	"fmt"

	protojson "google.golang.org/protobuf/encoding/protojson"
)

func (r Recording) Value() (driver.Value, error) {
	f := &r

	return protojson.MarshalOptions{UseProtoNames: true, EmitUnpopulated: true}.Marshal(f)
}

func (r *Recording) Scan(value interface{}) error {
	switch v := value.(type) {
	case []byte:
		return protojson.Unmarshal(v, r)
	case string:
		return protojson.Unmarshal([]byte(v), r)
	default:
		return fmt.Errorf("type assertion failed")
	}
}
