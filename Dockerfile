FROM golang:1.19-alpine

RUN set -ex \
	&& apk add --no-cache \
	build-base \
	linux-headers \
	protoc \
	py3-pip \
	py3-wheel \
	python3 \
	python3-dev \
	&& pip install --no-cache-dir \
	grpclib \
	protobuf \
	&& go install github.com/go-task/task/v3/cmd/task@latest
